<?php

/*

LARAVEL Debug bar: https://github.com/barryvdh/laravel-debugbar
*/

Route::get('/', 'NewsController@index')->name('homepage');



/*Route::get('/', function() {
	return view('welcome');
});*/

Auth::routes();

/*
Sukuriami 7 route'ai reikalingi CRUD operacijoms, sarasas:
https://laravel.com/docs/5.8/controllers#resource-controllers
*/

Route::get('naujienos/autorius/{id}', 'NewsController@authorIndex')->name('naujienos.author');

Route::resource('naujienos', 'NewsController');
Route::resource('comments', 'CommentController');
Route::resource('categories', 'CategoryController');

Route::get('search', 'SearchController@index')->name('search');

Route::get('/home', 'HomeController@index')->name('home');
