<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\NewsItem;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/


/* Visus faker objekto parametrus galime rasti: https://github.com/fzaninotto/Faker */

$factory->define(NewsItem::class, function (Faker $faker) {

	$paragraphs = $faker->paragraphs(3, false);

	$result = "";

	foreach ($paragraphs as $paragraph) {
		$result .= "<p>";
		$result .= $paragraph;
		$result .= "</p>";
	}

	Log::info($paragraphs);

	return [
		'title' => $faker->sentence(3),
		'content' => $result,
		'main_image' => $faker->imageUrl(500, 500, 'cats'),
		'user_id' => 1
	];

});
