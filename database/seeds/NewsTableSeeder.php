<?php

use Illuminate\Database\Seeder;

class NewsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // iskvieciu savo sukurta NewsItemFactory

		/* Budas su factories */

		factory(App\NewsItem::class, 50)->create()->each(function () {
			factory(App\NewsItem::class)->make();
		});


		/* Paprasis budas: */

		/*for($i = 0; $i < 100; $i++) {
			DB::table('news_items')->insert([
				'title' => 'Naujiena' . $i,
				'content' => Str::random(500),
				'main_image' => 'none',
				'user_id' => 1
			]);
		}*/
    }
}
