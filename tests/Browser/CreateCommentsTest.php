<?php

namespace Tests\Browser;

use Illuminate\Support\Facades\Log;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class CreateCommentsTest extends DuskTestCase {
	/**
	 * A Dusk test example.
	 *
	 * @return void
	 */
	public function testExample() {
		$this->browse( function ( Browser $browser ) {
			$browser->visit( 'https://www.15min.lt/naujiena/aktualu/lietuva/teismas-nagrines-n-venckienes-skunda-del-suemimo-56-1232370?comments' )
					//->pause( 1000 )
					//->click( '.vector-comments-small' )
					->pause( 1000 );

			$elements = $browser->elements( '.comment' );

			foreach ( $elements as $element ) {
				Log::info( $element );
			}

		} );
	}
}
