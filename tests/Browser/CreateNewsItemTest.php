<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class CreateNewsItemTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->browse(function (Browser $browser) {
        	// bandome sukurti naujiena kaip neprisijunges vartotojas
            $browser->visit('/naujienos/create')
				// jei as esu neprisijunges ir bandau sukurti naujiena,
				// tureciau buti nukreiptas i login
				->assertPathIs('/login');

			// bandome sukurti naujiena kaip prisijunges vartotojas

			$browser->loginAs(1)
				->visit('/naujienos/create')
				->assertSee("Sukurti naujiena")
				->pause(1000)
				->type('title', 'Testuoju naujienos sukurima')
				->type('content', 'Kazkoks tekstas')
				->click("#save")
				->assertSee("Sekmingai sukurta naujiena");
        });
    }
}