<div class="card mt-3">
    <div class="card-header">
        <div class="row">
            <div class="col-6">
                <a href="{{ route('naujienos.author', $comment->user->id) }}">
                    {{ $comment->user->name }}
                </a>

            </div>

            <div class="col-6">{{ $comment->created_at }}</div>

        </div>

    </div>
    <div class="card-body">
        {{-- Atvaizduoti komentaro autoriaus varda --}}
        {{-- Jei as esu komentaro autorius, tureciau galeti istrinti komentara --}}
        {{ $comment->comment_text }}
    </div>

    <div class="card-footer">
        <a href="#" class="btn btn-danger">Trinti</a>
        <a href="#" class="btn btn-warning">Redaguoti</a>
        <small>Laukeliai matomi tik komentaro autoriui arba adminui</small>
    </div>

</div>