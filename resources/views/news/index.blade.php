@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @if(isset($search))
                <div class="col-12">
                    Naujienos pagal uzklausa: {{ $search }}
                </div>
            @else
                <div class="col-12">
                   Visos naujienos
                </div>
            @endif

            @foreach($news as $newsItem)
                <div class="col-6 mt-3">
                    <img src="{{ $newsItem->main_image }}" />

                    <h4>
                        {{ $newsItem->title }}
                        {{-- Komentaru kiekis teisingas budas: --}}

                        <a href="{{ route('naujienos.show', $newsItem->id) }}#comments">
                            ({{ $newsItem->comments->count() }})
                        </a>


                        {{-- Komentaru kiekis neteisingas budas: --}}
                        {{--({{ count($newsItem->comments) }})--}}
                    </h4>

                    <div>
                        Autorius:
                        <a href="{{ route('naujienos.author', $newsItem->user_id) }}">
                            {{ $newsItem->user->fullName() }}
                        </a>

                        Kategorija:
                        @foreach($newsItem->categories  as $category)
                            {{ $category->name }}
                        @endforeach

                    </div>

                    <p>
                        {!! $newsItem->execerpt(20) !!}
                    </p>

                    <div>
                        <a href="{{ route('naujienos.show', $newsItem->id) }}">
                            Skaityti daugiau
                        </a>
                    </div>
                    @auth
                        {{-- Scenarijus kai esi prisijunges --}}
                        <form method="post" action="{{ route('naujienos.destroy', $newsItem->id) }}">
                            @csrf
                            {{-- Kadangi naudojame resource controlleri, butinai turime nurodyti
                                metoda delete
                            --}}
                            @method('delete')

                            <input type="submit" class="btn-danger btn" value="Trinti">
                        </form>
                    @else
                        {{-- Scenarijus kai esi neprisijunges--}}
                    @endauth




                    {{-- Tikrinimas is kito galo --}}
                    @guest
                        {{--kazka rodau jei esi neprisijunges----}}
                    @else
                        {{-- kazka rodau jei esi prisijunges--}}
                    @endguest


                </div>
            @endforeach

            @if(count($news) == 0)
                <div class="col-12">
                    <h1>Nieko nerasta.... </h1>
                </div>

            @endif

            {{-- Paginacija --}}

            {{ $news->links() }}
        </div>
    </div>
@endsection
