@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12">
                Autorius: {{ $user->name }}
            </div>

            <div class="col-6">
                <form method="get">
                    <select name="order">
                        <option value="title">Pavadinimas</option>
                        <option value="created_at">Sukurimo data</option>
                    </select>

                    <input type="submit" value="rikiuoti" />
                </form>
                <h2>Autoriaus naujienos</h2>
                @foreach($user->news($order)->get() as $newsItem)
                    <div class="col-6 mt-3">
                        <h4>
                            {{ $newsItem->title }}
                        </h4>

                        <div>
                            Autorius:
                            <a href="{{ route('naujienos.author', $newsItem->user_id) }}">
                                {{ $newsItem->user->email }}
                            </a>

                        </div>

                        <div>
                            <a href="{{ route('naujienos.show', $newsItem->id) }}">
                                Skaityti daugiau
                            </a>
                        </div>

                    </div>
                @endforeach
            </div>

            <div class="col-6">
                autoriaus parasytu simboliu kiekis: {{ $user->totalCharacters() }} simboliai


                <h3>Vartotojo komentarai</h3>

                @foreach($user->comments as $comment)
                    @include('comments.singleComment')
                @endforeach
            </div>


        </div>
    </div>
@endsection
