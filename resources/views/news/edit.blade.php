@extends('layouts.app')

@section('content')
    <form method="post" action="{{ route('naujienos.update', $newsItem->id) }}">
        @method('put')
        <div class="container">
            <div class="row">
                <div class="col-8">
                    <h1>Redaguoti naujiena</h1>


                    @csrf

                    <input class="form-control" value="{{ $newsItem->title }}" name="title"/>

                    <textarea id="content" name="content" class="form-control">{{ $newsItem->content }}</textarea>

                    <input type="submit" id="save" class="btn btn-success"/>
                </div>

                <div class="col-4">
                    <h3>Naujienos kategorijos</h3>
                    @foreach($categories as $category)
                        @if(in_array($category->id, $selectedCategories))
                            <input type="checkbox" checked name="category[]" value="{{ $category->id }}">
                            {{ $category->name }}
                            @else
                            <input type="checkbox" name="category[]" value="{{ $category->id }}">
                            {{ $category->name }}
                            @endif

                    @endforeach

                </div>
            </div>
        </div>
    </form>

    <script>
        // noredami pasinaudoti ckeditor, naudojame CKEDITOR.replace funkcija
        // kaip parametra perduodame textarea elemento ID

        // PAPILDOMAI: layouts/app.blade.php faile nuimti defer attributa nuo app.js failo include'o
        CKEDITOR.replace('content');
    </script>
@endsection
