@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12">
                <img src="{{ $newsItem->main_image }}" />
                <h1>{{ $newsItem->title }}</h1>
                Kategorija:
                @foreach($newsItem->categories  as $category)
                    {{ $category->name }}
                @endforeach


                <a href="{{ route('naujienos.edit', $newsItem->id) }}" class="btn btn-warning">
                    Redaguoti naujiena
                </a>
            </div>
            <div class="col-6 mt-3">
                <h4>
                    {{ $newsItem->title }}
                </h4>

                <p>
                    {{-- Stilizuota ir formatuota teksta isvedame su tokia sintakse: --}}
                    {!! $newsItem->content !!}
                </p>

            </div>

            <div class="col-12" id="comments">
                <hr>
                <h3>Komentarai</h3>

                @auth
                    <form action="{{ route('comments.store') }}" method="post">
                        @csrf

                        <div class="form-group">
                            <textarea class="form-control" placeholder="Iveskite komentara" required
                                      name="comment_text"></textarea>
                        </div>
                        {{-- Perduodame kuriai naujienai priklauso komentaras --}}
                        <input type="hidden" name="news_id" value="{{ $newsItem->id }}">

                        <input type="submit" class="btn btn-success" value="Rasyti komentara"/>
                    </form>
                @else
                    <p>Noredami rasyti komentarus, <a href="{{ route('login') }}">prisijunkite</a></p>
                @endauth
            </div>

            <div class="col-12">
                <hr>

                @foreach($newsItem->comments as $comment)
                    @include('comments.singleComment')
                @endforeach
            </div>
        </div>
    </div>

    <script>
        CKEDITOR.replace('comment_text');
    </script>
@endsection
