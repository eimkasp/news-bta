@extends('layouts.app')

@section('content')
    <form method="post" action="{{ route('naujienos.store') }}">
        <div class="container">
            <div class="row">
                <div class="col-8">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <h1>Sukurti naujiena</h1>


                    @csrf

                    <input class="form-control" value="{{ old('title') }}" name="title"/>

                    <textarea id="content" name="content" class="form-control">{{ old('content') }}</textarea>

                    <input type="submit" id="save" class="btn btn-success"/>
                </div>

                <div class="col-4">
                    <h3>Naujienos kategorijos</h3>

                    {{--<select name="category" class="form-control">
                        @foreach($categories as $category)
                            <option value="{{ $category->id }}">
                                {{ $category->name }}
                            </option>
                        @endforeach
                    </select>--}}


                    @foreach($categories as $category)
                        <input type="checkbox" name="category[]" value="{{ $category->id }}">
                        {{ $category->name }}
                    @endforeach

                </div>
            </div>
        </div>
    </form>

    <script>
        // noredami pasinaudoti ckeditor, naudojame CKEDITOR.replace funkcija
        // kaip parametra perduodame textarea elemento ID

        // PAPILDOMAI: layouts/app.blade.php faile nuimti defer attributa nuo app.js failo include'o
        CKEDITOR.replace('content');
    </script>
@endsection
