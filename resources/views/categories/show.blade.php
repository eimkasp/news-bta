@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1>
                    {{ $category->name }}
                </h1>
            </div>

            @foreach($category->news as $newsItem)
                <div class="col-6">
                    <a href="{{ route('naujienos.show', $newsItem->newsItem->id) }}">
                        {{ $newsItem->newsItem->title }}
                    </a>
                    <div>
                        Autorius: {{ $newsItem->newsItem->user->email }}
                    </div>

                </div>
            @endforeach
        </div>
    </div>
@endsection